/*
 *
 * DLLCaller bY psych0naut
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *	
 *                      _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
 *                     /    Very-Special Gr33tZ to     \
 *                    /                                 \     
 *                   /    weltenstrum (shizo-c++-god)    \
 *                  |         Chazwazza, lyan, ocz        |
 *                  |            and the usuals           |
 *                  |                                     |
 *                  | bY psych0naut (luvs ASCII and CATZ) |
 *                 /._ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _| 
 *          /\-/\ /./
 *         /a a  \               _
 *        =\ Y  =/-~~~~~~-,_____/ )
 *          '^--' teh one  ______/
 *            \ and only  /
 *            ||  |---'\  \
 *           (_(__|   ((__|
 * 
 *	
 */

class DLLCaller
{
	private:
	
		HMODULE hModule;

	public:

		DLLCaller(LPCSTR lpModuleName)
		{
			this->hModule = GetModuleHandle(lpModuleName) ? GetModuleHandle(lpModuleName) : LoadLibrary(lpModuleName);
		}
		
		/* Functions with Args */
			
		template <typename RET, typename ...ARGS>
		RET CallDLLFunction(LPCSTR lpFunctionName, int iCallingConvention, ARGS... arguments)
		{	
			if(iCallingConvention == 0)
			{
				RET(__cdecl *execute)(ARGS...) = NULL;
				execute = (RET(__cdecl *)(ARGS...))GetProcAddress(this->hModule, lpFunctionName);
				return execute(std::forward<ARGS>(arguments)...);
			}
			else if(iCallingConvention == 1)
			{	
				RET(__stdcall *execute)(ARGS...) = NULL;
				execute = (RET(__stdcall *)(ARGS...))GetProcAddress(this->hModule, lpFunctionName);
				return execute(std::forward<ARGS>(arguments)...);
			}

			/* 
				...
				Implement some more if you want 
			*/
		}

		/* Functions without Args */
				
		template <typename RET>
		RET CallAPIFunction(LPCSTR lpFunctionName, int iCallingConvention)
		{
			if(iCallingConvention == 0)
			{
				RET(__cdecl *execute)() = NULL;
				execute = (RET(__cdecl *)())GetProcAddress(this->hModule, lpFunctionName);
				return execute();
			}
			else if(iCallingConvention == 1)
			{	
				RET(__stdcall *execute)() = NULL;
				execute = (RET(__stdcall *)())GetProcAddress(this->hModule, lpFunctionName);
				return execute();
			}
		}
};