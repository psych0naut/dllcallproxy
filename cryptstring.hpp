/*
 *
 * CryptString bY someone from teh internetz
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 */

#include <string>
 
template<typename STR, STR... chars>
class CryptString
{
	private:
	
		/* Decryption stuff - This will not be calculated at compile time! */
		
		STR DecryptChar(STR C)
		{
			return (((C - 1) ^ 0x55) ^ 0x12);
		}
		
		/* Encryption Stuff */
	
		STR *value;
		std::size_t char_iter;
		static const std::size_t length = sizeof...(chars);
	
		template<STR C>
		struct EncryptChar 
		{
			static char const value = (((C ^ 0x12) ^ 0x55) + 1);
		};
		
		template<typename ...CRYPTED_CHARS>
		void build_string(STR *dest, STR C, CRYPTED_CHARS... crypted_chars)
		{
			build_string(dest, C);
			build_string(dest, crypted_chars...);
		}
	 
		void build_string(STR *dest, STR C)
		{
			dest[char_iter++] = C;
		}
	 
	public:

		CryptString()
		{
			this->char_iter = 0;
			this->value = new STR[this->length + 1];
			this->build_string(this->value, EncryptChar<chars>::value...);
		} 

		~CryptString()
		{
			delete[] this->value;
		}

		const STR *Value()
		{			
			std::basic_string<STR> decrypted;

			for(int i = 0; i < length; i++)
			decrypted.push_back(this->DecryptChar(this->value[i]));
	 
			return decrypted.c_str();
		}
		
		std::basic_string<STR> ValueStlString()
		{			
			std::basic_string<STR> decrypted;

			for(int i = 0; i < length; i++)
			decrypted.push_back(this->DecryptChar(this->value[i]));
	 
			return decrypted;
		}

		std::size_t Length()
		{
			return this->length;
		}
};