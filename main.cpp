// VS doesn't support this - Compile with G++ and -std=c++0x

#include <windows.h>
#include <iostream>
#include "dllcaller.hpp"
#include "cryptstring.hpp"

#define TO_STRING(x) #x
#define CHAR_TMPL_12(x) TO_STRING(x)[0] , TO_STRING(x)[1] , TO_STRING(x)[2] , TO_STRING(x)[3] , TO_STRING(x)[4] , TO_STRING(x)[5] , TO_STRING(x)[6] , TO_STRING(x)[7]  , TO_STRING(x)[8] , TO_STRING(x)[9] , TO_STRING(x)[10] , TO_STRING(x)[11]

int main()
{
	DLLCaller User32Proxy("user32.dll");
	std::cout << User32Proxy.CallDLLFunction<int, HWND, const char*, const char*, UINT>("MessageBoxA", 1, 0, "Text", "Title", 0) << std::endl;
	
	DLLCaller Kernel32Proxy("kernel32.dll");
	CryptString<char, CHAR_TMPL_12(GetErrorMode)> str_GetErrorMode;
	std::cout << Kernel32Proxy.CallAPIFunction<DWORD>(str_GetErrorMode.Value(), 1) << std::endl; 
	
	return 0;
}